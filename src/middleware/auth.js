const sendAuthErrorResponse = res => {
  res.status(401).json({error: 'Please authenticate'});
};

const auth = (req, res, next) => {
  if (!process.env.TOKEN) {
    next();
    return;
  }

  const token = req.header('authorization');

  if (!token) {
    sendAuthErrorResponse(res);
    return;
  }

  if (process.env.TOKEN !== token.replace('Bearer ', '')) {
    sendAuthErrorResponse(res);
    return;
  }

  next();
};

module.exports = auth;
