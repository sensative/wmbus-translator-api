const WMBUS_DECODER = require('iobroker.wireless-mbus/lib/wmbus_decoder');

const translate = (telegram, key, callback) => {
  const isEncrypted = key !== undefined;

  try {
    const decoder = new WMBUS_DECODER(() => { }, false);
    decoder.parse(telegram, isEncrypted, key, (decoderError, decoderResponse) => {
      if (decoderError) {
        callback(decoderError, undefined);
        return;
      }
      callback(undefined, decoderResponse);
    });
  } catch (error) {
    const errorMessage = {message: error.message, error};
    callback(errorMessage, undefined);
  }
};

module.exports = {translate};
