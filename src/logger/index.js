const winston = require('winston');

const logConfiguration = {
  transports: [
      new winston.transports.Console()
  ],
  format: winston.format.combine(
      winston.format.timestamp({
         format: 'MMM-DD-YYYY HH:mm:ss'
     }),
      winston.format.printf(info => `${info.level}: ${[info.timestamp]}: ${info.message}`),
  )
};

const logger = winston.createLogger(logConfiguration);

const info = msg => {
  logger.info(msg);
}

const debug = (msg) => {
  logger.debug(msg);
}

const error = (msg) => {
  logger.error(msg);
}

module.exports = {info, error, debug}
