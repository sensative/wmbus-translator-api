const express = require('express');
const translateRouter = require('./routers/translator/index');
const logger = require('./logger');

const PORT = process.env.PORT || 3000;
const HOST = process.env.HOST || '0.0.0.0';
let server;

const initExpress = () => {
  logger.info('### Starting Express HTTP Server ###');
  const app = express();
  app.use(express.json());
  app.use(translateRouter);

  return new Promise(resolve => {
    server = app.listen(PORT, HOST, () => {
      logger.info(`Listening on port: ${PORT}`);
      if (process.env.TOKEN) {
        logger.debug('Auth token provided, will be needed to make request to the service');
      }
      logger.info('%%% Express HTTP Server CONNETED %%%');
      resolve();
    });
    server.on('error', e => {
      logger.error('Express HTTP server error:', e.stack);
      process.exit(1);
    });
  });
};

const terminateExpress = async () => {
  logger.info('express stopping...');
  if (server) {
    await new Promise(resolve => {
      logger.info('express stopped!');
      server.close(resolve);
    });
  }
};

const start = async () => {
  await initExpress();
};

const stop = async () => {
  await terminateExpress();
};

module.exports = {start, stop};
