const wmbusTranslator = require('../../wmbus-translator');
const logger = require('../../logger');

const translate = (req, res) => {
  const telegram = req?.body?.telegram
  const key = req?.body?.key

  if (!telegram) {
    const message = "Expected field 'telegram' was not defined"
    res.status(400).json({
      error: {
        message
      }
    })
    logger.error(`Err: ${message}`)
    return;
  }

  wmbusTranslator.translate(telegram, key, (translatorError, translatorResult) => {
    if (translatorError) {
      res.status(400).json({
        error: {
          message: `An error occurred while translating the provided telegram`,
          translatorError
        }
      });
      //todo: log!
      logger.error('Err:', {
        telegram, key, translatorError: translatorError
      });
      return;
    }
    res.status(200).json(translatorResult);
  });
}

module.exports = translate;
