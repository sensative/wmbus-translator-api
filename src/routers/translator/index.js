const express = require('express');
const auth = require('../../middleware/auth');
const translate = require('./routes');

const translateRouter = express.Router();

translateRouter.post('/translator', auth, translate);

module.exports = translateRouter;
