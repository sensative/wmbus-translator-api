const {start, stop} = require('./express');

start();

['SIGINT', 'SIGTERM'].forEach(signal => {
  process.on(signal, () => {
    stop();
  });
});
