# Wmbus Translator Api

A api that integrates a wmbus decoder

# Use

```text
$ docker-compose up --build
```

# Authentication
Authentication is by default disabled. To enable authentication you need to set the environment variable "TOKEN" to a value of your choosing


# Example
* Basic post request to translate telegram
```text
curl -X POST \
  'http://localhost:3000/translator' \
  --header 'Accept: */*' \
  --header 'User-Agent: Thunder Client (https://www.thunderclient.com)' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "telegram": "{{telegram}}"
}'
```
```text
{
  "deviceInformation": {
    "AccessNumber": 130,
    "Id": "67991306",
    "Manufacturer": "LUG",
    "Medium": "Heat",
    "Status": 16,
    "StatusString": "temporary error",
    "Version": 7,
    "Address": "a732061399670704"
  },
  "dataRecord": [
      ...
  ]
}
```

<br />

* Enrypted telegram with encryption key

```text
curl -X POST \
  'http://http://localhost:3000/translator' \
  --header 'Accept: */*' \
  --header 'User-Agent: Thunder Client (https://www.thunderclient.com)' \
  --header 'Authorization: super_secret' \
  --header 'Authorization: Bearer super_secret_token' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "telegram": "{{telegram}}",
  "key": "{{key}}"
}'
```
```text
{
  "deviceInformation": {
    "AccessNumber": 42,
    "Id": "12345678",
    "Manufacturer": "ELS",
    "Medium": "Gas",
    "Status": 0,
    "StatusString": "no errors",
    "Version": 51,
    "Address": "9315785634123303"
  },
  "dataRecord": [
    ...
  ]
}
```


* Send request with authorization token provided
```text
curl -X POST \
  'http://localhost:3000/translator' \
  --header 'Accept: */*' \
  --header 'User-Agent: Thunder Client (https://www.thunderclient.com)' \
  --header 'Authorization: Bearer super_secret_token' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "telegram": "{{telegram}}"
}'
```
```text
{
  "deviceInformation": {
    "AccessNumber": 130,
    "Id": "67991306",
    "Manufacturer": "LUG",
    "Medium": "Heat",
    "Status": 16,
    "StatusString": "temporary error",
    "Version": 7,
    "Address": "a732061399670704"
  },
  "dataRecord": [
      ...
  ]
}
```
