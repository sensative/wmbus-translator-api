'use strict';

const assert = require('assert');
const wmbusTranslator = require('../src/wmbus-translator');
const testData = require('./test-data');

describe('Test wmbus translator', () => {
  it('Should give back translated data from provided telegram', () => {
    const telegram = '2C44A7320613996707047A821000202F2F0C06000000000C14000000000C22224101000B5A4102000B5E4000F05E';
    wmbusTranslator.translate(telegram, undefined, (_, translatorResult) => {
      assert.deepStrictEqual(translatorResult, testData);
    });
  });
  it('Should give back translated data from provided encrypted telegram', () => {
    const telegram = '2E44931578563412330333637A2A0020255923C95AAA26D1B2E7493BC2AD013EC4A6F6D3529B520EDFF0EA6DEFC955B29D6D69EBF3EC8A';
    const key = '0102030405060708090A0B0C0D0E0F11';
    wmbusTranslator.translate(telegram, key, (_, translatorResult) => {
      assert(translatorResult);
    });
  });
  it('Should return error given to short telegram', () => {
    const telegram = '2C44A7320613996707047A821000202F2F0C06000000000C14000000000C22224101000B5A4102000';
    wmbusTranslator.translate(telegram, undefined, translatorError => {
      assert.deepStrictEqual(translatorError, {
        message: 'application layer message too short, expected 45, got 40 bytes', code: 12
      });
    });
  });
  it('Should return error given corrupt telegram', () => {
    const telegram = '2C';
    wmbusTranslator.translate(telegram, undefined, translatorError => {
      assert.strictEqual(translatorError.message, 'Attempt to access memory outside buffer bounds');
    });
  });
  it('Should throw error given corrupt key', () => {
    const telegram = '2E44931578563412330333637A2A0020255923C95AAA26D1B2E7493BC2AD013EC4A6F6D3529B520EDFF0EA6DEFC955B29D6D69EBF3EC8A';
    const key = '01020304050';
    wmbusTranslator.translate(telegram, key, translatorError => {
      assert.deepStrictEqual(translatorError, {
        message: 'encrypted message and no aeskey provided', code: 9
      });
    });
  });
  it('Should throw error given no key with encrypted key provided', () => {
    const telegram = '2E44931578563412330333637A2A0020255923C95AAA26D1B2E7493BC2AD013EC4A6F6D3529B520EDFF0EA6DEFC955B29D6D69EBF3EC8A';
    const key = '';
    wmbusTranslator.translate(telegram, key, translatorError => {
      assert.deepStrictEqual(translatorError, {
        message: 'encrypted message and no aeskey provided', code: 9
      });
    });
  });
});
