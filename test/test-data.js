const testData = {
  deviceInformation: {
    AccessNumber: 130,
    Id: '67991306',
    Manufacturer: 'LUG',
    Medium: 'Heat',
    Status: 16,
    StatusString: 'temporary error',
    Version: 7,
    Address: 'a732061399670704'
  },
  dataRecord: [
    {
      number: 1,
      value: 0,
      unit: 'Wh',
      type: 'VIF_ENERGY_WATT',
      description: 'Energy',
      tariff: 0,
      storageNo: 0,
      devUnit: 0,
      functionFieldText: 'Instantaneous value',
      functionField: 0
    },
    {
      number: 2,
      value: 0,
      unit: 'm³',
      type: 'VIF_VOLUME',
      description: 'Volume',
      tariff: 0,
      storageNo: 0,
      devUnit: 0,
      functionFieldText: 'Instantaneous value',
      functionField: 0
    },
    {
      number: 3,
      value: 14122,
      unit: 'h',
      type: 'VIF_ON_TIME',
      description: 'On Time',
      tariff: 0,
      storageNo: 0,
      devUnit: 0,
      functionFieldText: 'Instantaneous value',
      functionField: 0
    },
    {
      number: 4,
      value: '24.1',
      unit: '°C',
      type: 'VIF_FLOW_TEMP',
      description: 'Flow Temperature',
      tariff: 0,
      storageNo: 0,
      devUnit: 0,
      functionFieldText: 'Instantaneous value',
      functionField: 0
    },
    {
      number: 5,
      value: -4,
      unit: '°C',
      type: 'VIF_RETURN_TEMP',
      description: 'Return Temperature',
      tariff: 0,
      storageNo: 0,
      devUnit: 0,
      functionFieldText: 'Instantaneous value',
      functionField: 0
    }
  ]
};

module.exports = testData;
