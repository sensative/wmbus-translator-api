const sinon = require('sinon');
const assert = require('assert');

const basicRes = () => {
  const json = sinon.stub();
  const set = sinon.stub();
  const status = () => ({json});
  status.json = json;
  return {status: sinon.spy(status), json, set};
};

const basicReq = (body, headers) => {
  const header = h => headers[h];
  return {body, header};
};

const assertStatusIsCalledWith = (res, statusCode) => {
  assert(res.status.called, 'status should be called');
  const actualCode = res.status.args[0][0];
  assert(res.status.calledOnceWith(statusCode), `status should be called with code ${statusCode}, not ${actualCode}`);
};

const assertStatusNotCalled = res => {
  assert(res.status.notCalled);
};

const assertJsonIsCalled = res => {
  assert(res.json.called, 'json should be called');
};

const assertNextIsCalled = next => {
  assert(next.called, 'Next should be called');
};

const assertNextNotCalled = next => {
  assert(next.notCalled, 'Next should not be called');
};

module.exports = {
  basicRes,
  basicReq,
  assertStatusIsCalledWith,
  assertStatusNotCalled,
  assertJsonIsCalled,
  assertNextIsCalled,
  assertNextNotCalled
};
