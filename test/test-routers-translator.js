const translator = require('../src/routers/translator/routes');
const {basicRes, basicReq, assertStatusIsCalledWith} = require('./utils');


describe('Test /translator', () => {
  describe('Telegram not encrypted', () => {
    it('Should get status 200', () => {
      const body = {
        telegram: '2C44A7320613996707047A821000202F2F0C06000000000C14000000000C22224101000B5A4102000B5E4000F05E'
      };
      const req = basicReq(body);
      const res = basicRes();
      translator(req, res);
      assertStatusIsCalledWith(res, 200);
    });

    it('Should get status 400, no telegram', () => {
      const req = basicReq();
      const res = basicRes();
      translator(req, res);
      assertStatusIsCalledWith(res, 400);
    });
  });

  describe('Telegram encrypted', () => {
    it('Should get status 200, encrypted telegram', () => {
      const body = {
        telegram: '2E44931578563412330333637A2A0020255923C95AAA26D1B2E7493BC2AD013EC4A6F6D3529B520EDFF0EA6DEFC955B29D6D69EBF3EC8A',
        key: '0102030405060708090A0B0C0D0E0F11'
      };

      const req = basicReq(body);
      const res = basicRes();
      translator(req, res);
      assertStatusIsCalledWith(res, 200);
    });

    it('Should get status 400, no key provided', () => {
      const body = {
        telegram: '2E44931578563412330333637A2A0020255923C95AAA26D1B2E7493BC2AD013EC4A6F6D3529B520EDFF0EA6DEFC955B29D6D69EBF3EC8A',
      };
      const req = basicReq(body);
      const res = basicRes();
      translator(req, res);
      assertStatusIsCalledWith(res, 400);
    });

    it('Should get status 400, corrupted key provided', () => {
      const body = {
        telegram: '2C44A7320613996707047A821000202F2F0C06000000000C14000000000C22224101000B5A4102000B5E4000F05E',
        key: '0102030405060708090A0B0C0D0E0F11'
      };
      const req = basicReq(body);
      const res = basicRes();
      translator(req, res);
      assertStatusIsCalledWith(res, 400);
    });
  });
});
