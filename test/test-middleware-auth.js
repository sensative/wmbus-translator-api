const sinon = require('sinon');
const auth = require('../src/middleware/auth');
const utils = require('./utils');

const setTokenEnvironemntVairalble = token => {
  process.env.TOKEN = token;
};
const unsetTokenEnironmentVariable = () => {
  delete process.env.TOKEN;
};
describe('Test auth middleware', () => {
  let res;
  beforeEach(() => {
    res = utils.basicRes();
  });

  afterEach(() => {
    unsetTokenEnironmentVariable();
  });

  describe('Token enironment variable set', () => {
    const token = 'super_secret_token';
    beforeEach(() => {
      res = utils.basicRes();
      setTokenEnvironemntVairalble(token);
    });


    it('Should call next, correct token provided', () => {
      const next = sinon.spy();
      const header = {authorization: token};
      const req = utils.basicReq(undefined, header);
      auth(req, res, next);
      utils.assertNextIsCalled(next);
      utils.assertStatusNotCalled(res);
    });

    it('Should send status 401, wrong token provided', () => {
      const next = sinon.spy();
      const header = {authorization: 'not_token'};

      const req = utils.basicReq(undefined, header);
      auth(req, res, next);
      utils.assertNextNotCalled(next);
      utils.assertStatusIsCalledWith(res, 401);
    });

    it('Should send status 401, no token provided', () => {
      const next = sinon.spy();
      const header = {};

      const req = utils.basicReq(undefined, header);
      auth(req, res, next);
      utils.assertNextNotCalled(next);
      utils.assertStatusIsCalledWith(res, 401);
    });
  });

  describe('Token environment variable not set', () => {
    it('Should call next', () => {
      const next = sinon.spy();
      const req = utils.basicReq();
      auth(req, res, next);
      utils.assertNextIsCalled(next);
      utils.assertStatusNotCalled(res);
    });
  });
});
